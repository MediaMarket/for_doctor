from fastapi import FastAPI
from starlette.exceptions import HTTPException as StarletteHTTPException
from fastapi.exceptions import RequestValidationError, HTTPException
from starlette.responses import JSONResponse
from src.routes import weather_router
from src.models.error_model import ErrorModel
from src.db.db_worker import DBWorker

responses = {
    422: {'model': ErrorModel, 'description': 'Validation Error'},
    400: {'model': ErrorModel, 'description': 'Bad request'}
}

app = FastAPI(docs_url="/",
              redoc_url=None,
              title="Weather Info API",
              responses=responses)

app.include_router(weather_router.api, tags=["Get weather info block"])


@app.exception_handler(HTTPException)
@app.exception_handler(StarletteHTTPException)
def http_error_handler(request, ex):
    return JSONResponse(status_code=ex.status_code, content={"status": "error", "msg": ex.detail})


@app.exception_handler(RequestValidationError)
def validation_error_handler(request, ex):
    return JSONResponse(status_code=400, content={"status": "error", "msg": str(ex)})


@app.on_event("startup")
async def app_start():
    await DBWorker.init_connection()
