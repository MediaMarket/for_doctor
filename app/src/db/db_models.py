from sqlalchemy import Column, DateTime, String, Integer
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class WeatherInfo(Base):
    __tablename__ = "weather_info"

    id = Column(Integer, primary_key=True, autoincrement=True)
    date = Column(DateTime, nullable=False, index=True)
    city = Column(String, nullable=False, index=True)
    country_code = Column(String, nullable=False, index=True)
    info = Column(String, nullable=False)
