import os
from datetime import datetime
from sqlalchemy import select, and_
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from .db_models import Base, WeatherInfo


class DBWorker:
    """ Класс, обслуживающий работу с БД
        Сохраняет и получает данные о погоде """

    _db_engine = None

    @classmethod
    async def init_connection(cls):
        """ Инициализация БД для хранения ранее запрошенной информации о погода"""

        if not cls._db_engine:
            db_user = os.environ.get("DB_USER")
            db_pwd = os.environ.get("DB_PWD")
            db_host = os.environ.get("DB_HOST") or "localhost"
            db_name = os.environ.get("DB_NAME")
            connection_string = f"postgresql+asyncpg://{db_user}:{db_pwd}@{db_host}/{db_name}"
            cls._db_engine = create_async_engine(connection_string)
        async with cls._db_engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)

    async def add_weather_info(self, data, info):
        """ Добавление информации о погодв в БД """

        data_dict = data.dict()
        data_dict["date"] = datetime.strptime(data.date, "%d.%m.%YT%H:%M")
        async with AsyncSession(DBWorker._db_engine) as session:
            async with session.begin():
                new_weather_info = WeatherInfo(**data_dict, info=info)
                session.add(new_weather_info)
                session.commit()

    async def get_weather_info(self, data):
        """ Чтение информации о погоде из БД """

        dt = datetime.strptime(data.date, "%d.%m.%YT%H:%M")
        async with AsyncSession(DBWorker._db_engine) as session:
            result = await session.execute(select(WeatherInfo.info)
                                           .where(and_(WeatherInfo.date == dt,
                                                       WeatherInfo.city == data.city,
                                                       WeatherInfo.country_code == data.country_code)))
            return result.scalar()
