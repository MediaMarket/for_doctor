from fastapi.exceptions import HTTPException
from pydantic import BaseModel, Field, validator
from datetime import datetime


class QueryParamsModel(BaseModel):
    """ Модель для проверки входных query-параметров """

    country_code: str = Field(..., min_length=2, max_length=3)
    city: str = Field(..., min_length=2, regex=r"[A-Za-z]+")
    date: str = Field(...)

    @validator("date")
    def check_date(value: str):
        try:
            datetime.strptime(value, "%d.%m.%YT%H:%M")
        except ValueError as ve:
            raise HTTPException(status_code=422, detail=f"{ve}")
        return value


class WeatherModel(BaseModel):
    result: str


