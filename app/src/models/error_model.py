from typing import Optional
from pydantic import BaseModel


class ErrorModel(BaseModel):
    status: str
    msg: Optional[str]



