import json
from fastapi import APIRouter, Depends, HTTPException
from starlette.responses import JSONResponse
from src.models.weather_model import QueryParamsModel
from src.weather_controller import WeatherController

api = APIRouter()


@api.get("/weather")
async def get_weather_info(params: QueryParamsModel = Depends()):
    wc = WeatherController(params)
    weather_info = await wc.get_weather_info()
    return JSONResponse(json.loads(weather_info))
