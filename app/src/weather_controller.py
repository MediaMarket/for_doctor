import asyncio
import os
from urllib.error import HTTPError
from urllib.request import urlopen
from urllib.parse import quote_plus
from fastapi import HTTPException
from src.models.weather_model import QueryParamsModel
from src.db.db_worker import DBWorker


class WeatherController:

    _WEATHER_API = "https://api.openweathermap.org/data/2.5/weather?q={},{}&appid={}"

    def __init__(self, request_data: QueryParamsModel):
        self._request_data = request_data
        self._db_worker = DBWorker()
        self._api_key = os.environ.get("API_KEY")

    async def _get_response(self, api_url):
        try:
            data = urlopen(api_url).read()
        except HTTPError as er:
            raise HTTPException(status_code=er.code, detail=f"{er}")
        return data.decode(encoding="utf-8")

    async def _check_weather_in_db(self):
        return await self._db_worker.get_weather_info(self._request_data)

    async def _request_weather_from_api(self):
        url = self._WEATHER_API.format(
                    quote_plus(self._request_data.city),
                    quote_plus(self._request_data.country_code),
                    self._api_key)
        get_data_task = asyncio.create_task(self._get_response(url))
        return await get_data_task

    async def get_weather_info(self):
        """ Метод проверяет наличие информации о погоде в БД
            и запрашивает через API в случае ее отсутствия """

        weather_info = await self._check_weather_in_db()
        if not weather_info:
            weather_info = await self._request_weather_from_api()
            await self._db_worker.add_weather_info(self._request_data, info=weather_info)
        return weather_info



